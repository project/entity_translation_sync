<?php

namespace Drupal\Tests\entity_translation_sync\Functional;

use Drupal\Core\Language\LanguageInterface;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\user\UserInterface;

/**
 * Test that only allowed users access to the page.
 */
class EntityTranslationSyncUserAccessTest extends EntityTranslationSyncTestsBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'taxonomy',
    'entity_test',
    'language',
    'content_translation',
    'entity_translation_sync',
  ];

  /**
   * The vocabulary.
   *
   * @var \Drupal\taxonomy\Entity\Vocabulary
   */
  protected $vocabulary;

  /**
   * User without only view permissions.
   *
   * Used to check that if there aren't any sync permissions
   * then there is no access.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $anonymousUser;

  /**
   * User that can only synchronize taxonomy terms.
   *
   * Used to check that being able to synchronize another
   * entity type does not grant access to the others.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $synchronizerTaxonomyTermUser;

  /**
   * User that can synchronize the tested entity.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $synchronizerEntityTestUser;

  /**
   * {@inheritdoc}
   */
  protected function setupUsers() {
    parent::setupUsers();
    $this->anonymousUser = $this->drupalCreateUser($this->getEntityTestViewPermissions(), 'anonymous');
    $this->synchronizerTaxonomyTermUser = $this->drupalCreateUser(array_merge([
      'synchronize taxonomy_term translation',
    ], $this->getEntityTestViewPermissions()), 'synchronizer_taxonomy_term');
    $this->synchronizerEntityTestUser = $this->drupalCreateUser(array_merge([
      'synchronize entity_test_mul translation',
    ], $this->getEntityTestViewPermissions()), 'synchronizer_entity_test');
  }

  /**
   * {@inheritdoc}
   */
  protected function setupBundle() {
    parent::setupBundle();
    $this->vocabulary = Vocabulary::create([
      'name' => $this->randomMachineName(),
      'description' => $this->randomMachineName(),
      'vid' => mb_strtolower($this->randomMachineName()),
      'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
      'weight' => mt_rand(0, 10),
    ]);

    $this->vocabulary->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function getConfiguredEntityTypes(): array {
    return array_merge(parent::getConfiguredEntityTypes(), [
      'taxonomy_term' => [
        'bundles' => [
          $this->vocabulary->id() => [
            'fields' => [
              'name',
            ],
          ],
        ],
      ],
    ]);
  }

  /**
   * List of users needed to test its access.
   *
   * @return array<array<string|int>>
   *   All users that are needed to be checked.
   */
  public function dataProviderTestAccess() {
    return [
      'Access denied: Anonymous, entity synchronizable' => [
        'User without permissions can not sync nodes',
        'Test entity with sync access',
        'anonymous',
        403,
      ],
      'Access denied: Anonymous, entity not synchronizable' => [
        'User without permissions can not sync nodes',
        'Test entity without sync access',
        'anonymous',
        403,
      ],
      'Access denied: Taxonomy permissions, entity synchronizable' => [
        'User with taxonomy term permissions can not sync nodes',
        'Test entity with sync access',
        'synchronizer_taxonomy_term',
        403,
      ],
      'Access denied: Taxonomy permissions, entity not synchronizable' => [
        'User with taxonomy term permissions can not sync nodes',
        'Test entity without sync access',
        'synchronizer_taxonomy_term',
        403,
      ],
      'Access denied: Entity test permissions, entity synchronizable' => [
        'User with node permissions is able to sync',
        'Test entity with sync access',
        'synchronizer_entity_test',
        200,
      ],
      'Access denied: Entity test permissions, entity not synchronizable' => [
        'User with node permissions is able to sync',
        'Test entity without sync access',
        'synchronizer_entity_test',
        403,
      ],
      'Access denied: All permissions, entity synchronizable' => [
        'User with all permissions is able to sync',
        'Test entity with sync access',
        'synchronizer_full',
        200,
      ],
      'Access denied: All permissions, entity not synchronizable' => [
        'User with node permissions is able to sync',
        'Test entity without sync access',
        'synchronizer_full',
        403,
      ],
    ];
  }

  /**
   * Test that users can access only with the correct permissions.
   *
   * Only these permissions must allow synchronizing an entity:
   *  - synchronize any entity translation
   *  - synchronize <entity-type> translation.
   *
   * @dataProvider dataProviderTestAccess
   */
  public function testAccess(string $label, string $entity_title, string $user_name, int $expected_status_code) {
    $user = $this->getUserByName($user_name);
    $this->drupalLogin($user);
    $entity = $this->drupalGetEntityTestByLabel($entity_title);
    $this->drupalGet($entity->toUrl('drupal:entity-translation-sync'));
    $this->assertEquals($expected_status_code, $this->getSession()->getStatusCode(), $label);
    $this->drupalLogout();
  }

  /**
   * Get one of the users created during setup by its name.
   *
   * @param string $name
   *   User name.
   *
   * @return \Drupal\user\UserInterface
   *   User.
   */
  protected function getUserByName(string $name) {
    switch ($name) {
      case 'anonymous':
        return $this->anonymousUser;

      case 'synchronizer_taxonomy_term':
        return $this->synchronizerTaxonomyTermUser;

      case 'synchronizer_entity_test':
        return $this->synchronizerEntityTestUser;

      case 'synchronizer_full':
        return $this->synchronizerFullUser;

      default:
        throw new \InvalidArgumentException(sprintf('User with name "%s" does not exists."', $name));
    }
  }

}
