<?php

namespace Drupal\Tests\entity_translation_sync\Functional;

/**
 * Test that entity synchronization works properly.
 */
class EntityTranslationSyncTest extends EntityTranslationSyncTestsBase {

  /**
   * Assert is not possible to sync an entity without translations.
   */
  public function testSyncWithoutTranslations() {
    $this->drupalLogin($this->synchronizerFullUser);
    $entity = $this->drupalGetEntityTestByLabel('Test entity with sync access');
    $this->drupalGet($entity->toUrl('drupal:entity-translation-sync'));
    $this->assertSession()->pageTextContains('This entity do not have translation to sync its fields with.');
  }

  /**
   * Assert page is properly build when there are translations.
   */
  public function testSyncPageHasCorrectFields() {
    $this->createTranslations();
    $this->drupalLogin($this->synchronizerFullUser);
    $entity = $this->drupalGetEntityTestByLabel('Test entity with sync access');
    $this->drupalGet($entity->toUrl('drupal:entity-translation-sync'));

    // Check we can sync the entity.
    $this->assertSession()->pageTextNotContains('This entity do not have translation to sync its fields with.');

    // Check that the table appears with the correct headers.
    $this->assertSession()->pageTextContains('Attributes');
    $this->assertSession()->pageTextContains('English (Selected language)');
    $this->assertSession()->pageTextContains('French');
    $this->assertSession()->pageTextContains('Italian');

    // Check that there is a row for each translatable text field:
    $this->assertSession()->pageTextContains('Test translatable text-field (field_etsync_text)');
    $this->assertSession()->pageTextContains('Etsync text');
    $this->assertSession()->pageTextContains('Test translatable text-field 2 (field_etsync_text_2)');
    $this->assertSession()->pageTextContains('Etsync text 2');
    $this->assertSession()->pageTextContains('Test synchronizable text-field with restricted access (field_etsync_restricted_access)');
    $this->assertSession()->pageTextContains('Etsync restricted access');
    $this->assertSession()->pageTextNotContains('Test not synchronizable text-field (field_etsync_not)');
  }

  /**
   * Provides the examples to test different types of synchronization.
   *
   * The attempt is being able to ensure that an entity:
   *  - Can be synced from any of its available languages.
   *  - Can be translated to more than one language.
   */
  public function dataProviderTestSynchronization() {
    return [
      'Synchronization OK: one language, one field.' => [
        'en',
        [
          'field_etsync_text' => 'Etsync test changed',
        ],
        [
          'fr' => [
            'field_etsync_text',
          ],
        ],
        'Entity has been synchronized successfully to: French',
        [
          'fr' => [
            'field_etsync_text' => 'Etsync test changed',
            'field_etsync_text_2' => 'Etsync text 2',
            'field_etsync_not' => 'Etsync text not',
          ],
          'it' => [
            'field_etsync_text' => 'Etsync text',
            'field_etsync_text_2' => 'Etsync text 2',
            'field_etsync_not' => 'Etsync text not',
          ],
        ],
      ],
      'Synchronization OK: multiple languages, multiple fields.' => [
        'fr',
        [
          'field_etsync_text' => 'Etsync text changed from FR',
          'field_etsync_text_2' => 'Etsync text 2 changed from FR',
          'field_etsync_restricted_access' => 'Etsync text restricted changed from FR',
        ],
        [
          'en' => [
            'field_etsync_text',
          ],
          'it' => [
            'field_etsync_text',
            'field_etsync_text_2',
            'field_etsync_restricted_access',
          ],
        ],
        'Entity has been synchronized successfully to: English, Italian',
        [
          'en' => [
            'field_etsync_text' => 'Etsync text changed from FR',
            'field_etsync_text_2' => 'Etsync text 2',
          ],
          'it' => [
            'field_etsync_text' => 'Etsync text changed from FR',
            'field_etsync_text_2' => 'Etsync text 2 changed from FR',
            'field_etsync_restricted_access' => 'Etsync text restricted changed from FR',
          ],
        ],
      ],
      'Synchronization KO: no language selected, no fields selected.' => [
        'en',
        [],
        [],
        'No attributes has been selected to sync.',
        [
          'fr' => [
            'field_etsync_text' => 'Etsync text',
            'field_etsync_text_2' => 'Etsync text 2',
            'field_etsync_not' => 'Etsync text not',
          ],
          'it' => [
            'field_etsync_text' => 'Etsync text',
            'field_etsync_text_2' => 'Etsync text 2',
            'field_etsync_not' => 'Etsync text not',
          ],
        ],
      ],
    ];
  }

  /**
   * Assert is possible to sync an entity value among its translations.
   *
   * @dataProvider dataProviderTestSynchronization
   */
  public function testSynchronization(string $selected_language, array $entity_fields_changed, array $entity_attributes_sync, string $message_to_check, array $entity_values_to_check) {
    $this->createTranslations();
    $entity = $this->drupalGetEntityTestByLabel('Test entity with sync access');

    if ($entity->language()->getId() == $selected_language) {
      $selected_entity = $entity;
    }
    else {
      $selected_entity = $entity->getTranslation($selected_language);
    }

    foreach ($entity_fields_changed as $field_name => $value) {
      $selected_entity->set($field_name, $value);
    }
    $selected_entity->save();

    $this->drupalLogin($this->synchronizerFullUser);
    $this->drupalGet($selected_entity->toUrl('drupal:entity-translation-sync'));

    foreach ($entity_attributes_sync as $langcode => $fields) {
      foreach ($fields as $field_name) {
        $this->getSession()->getPage()->checkField(sprintf('entity_attributes_sync[%s][%s]', $langcode, $field_name));
      }
    }

    $this->submitForm([], 'Sync');

    $this->assertSession()->pageTextContains($message_to_check);

    $entity = $this->drupalGetEntityTestByLabel('Test entity with sync access', TRUE);

    foreach ($entity_values_to_check as $translation_langcode => $fields_to_check) {
      $translation = $entity->getTranslation($translation_langcode);
      foreach ($fields_to_check as $field_name => $field_value) {
        $this->assertEquals($translation->get($field_name)->get(0)->getString(), $field_value);
      }
    }

  }

  /**
   * Create translations for the entity created in a test.
   */
  protected function createTranslations() {
    /** @var \Drupal\Core\Entity\TranslatableInterface $entity */
    $entity = $this->drupalGetEntityTestByLabel('Test entity with sync access');
    /** @var \Drupal\Core\Entity\EntityTypeInterface $entity_type */
    $entity_type = $entity->getEntityType();
    foreach (['fr', 'it'] as $langcode) {
      $entity
        ->addTranslation($langcode, [
          $entity_type->getKey('name') => sprintf('%s %s translation', $entity->label(), $langcode),
          'field_etsync_text' => 'Etsync text',
          'field_etsync_text_2' => 'Etsync text 2',
          'field_etsync_not' => 'Etsync text not',
          'field_etsync_restricted_access' => 'Etsync restricted access',
        ]);
    }
    $entity->save();
  }

  /**
   * Test fields with restricted access are not editable.
   */
  public function testFieldsWithRestrictedAccess() {
    $this->createTranslations();
    $field_selector = sprintf('entity_attributes_sync[%s][%s]', 'fr', 'field_etsync_restricted_access');
    $user = $this->drupalCreateUser(array_merge($this->getEntityTestViewPermissions(), ['synchronize any entity translation']));
    $entity = $this->drupalGetEntityTestByLabel('Test entity with sync access');

    $this->drupalLogin($user);
    $this->drupalGet($entity->toUrl('drupal:entity-translation-sync'));

    $this->assertSession()->pageTextContains('Test synchronizable text-field with restricted access (field_etsync_restricted_access)');
    $this->assertSession()->pageTextContains('Etsync restricted access');

    $field = $this->getSession()->getPage()->findField($field_selector);
    $this->assertEmpty($field);

    $this->drupalLogin($this->synchronizerFullUser);
    $this->drupalGet($entity->toUrl('drupal:entity-translation-sync'));
    $this->assertSession()->pageTextContains('Test synchronizable text-field with restricted access (field_etsync_restricted_access)');
    $this->assertSession()->pageTextContains('Etsync restricted access');
    $field = $this->getSession()->getPage()->findField($field_selector);
    $this->assertNotEmpty($field);
  }

}
