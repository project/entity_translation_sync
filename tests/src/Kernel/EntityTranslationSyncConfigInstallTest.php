<?php

namespace Drupal\Tests\entity_translation_sync\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Base test to check config is installed properly.
 */
class EntityTranslationSyncConfigInstallTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'entity_translation_sync',
  ];

  /**
   * Test that configuration is installed properly.
   */
  public function testConfigInstall() {
    $this->installConfig(['entity_translation_sync']);
    $config = $this->config('entity_translation_sync.settings');
    $entity_types = $config->get('entity_types');
    $this->assertIsArray($entity_types);
    $this->assertEmpty($entity_types);
  }

}
