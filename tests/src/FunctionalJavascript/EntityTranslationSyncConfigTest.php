<?php

namespace Drupal\Tests\entity_translation_sync\FunctionalJavascript;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Test that entity synchronization works properly.
 */
class EntityTranslationSyncConfigTest extends WebDriverTestBase {

  /**
   * User able to configure the module.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $administratorUser;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'entity_test',
    'language',
    'content_translation',
    'entity_translation_sync',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Entity type used for tests.
   *
   * @var string
   */
  protected $entityTypeId = 'entity_test_mul';

  /**
   * Entity bundle used for tests.
   *
   * @var string
   */
  protected $bundle = 'entity_test_mul';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->administratorUser = $this->drupalCreateUser([
      'administer site configuration',
    ]);
    \Drupal::service('content_translation.manager')->setEnabled($this->entityTypeId, $this->bundle, TRUE);

    FieldStorageConfig::create([
      'field_name' => 'field_etsync_text',
      'type' => 'string',
      'entity_type' => $this->entityTypeId,
      'cardinality' => 1,
      'translatable' => 1,
    ])->save();
    FieldConfig::create([
      'entity_type' => $this->entityTypeId,
      'field_name' => 'field_etsync_text',
      'bundle' => $this->bundle,
      'label' => 'Test translatable text-field',
      'translatable' => 1,
    ])->save();

  }

  /**
   * Test configuration page works okay in a site with translatable entities.
   *
   * Check the page has the correct fields, the correct form states, and
   * when it is submitted the values are saved with the needed structure.
   */
  public function testConfigurationWithTranslatableEntities() {
    $this->drupalLogin($this->administratorUser);
    $this->drupalGet('/admin/config/regional/entity-translation-sync');
    $this->assertSession()->pageTextContains('Entity Translation Sync settings');
    $this->assertSession()->pageTextNotContains('There are not translatable entities enabled in the site.');

    $this->assertSession()->pageTextContains('Enabled entity types');
    $this->assertSession()->pageTextContains('Test entity - data table');

    $this->assertSession()->pageTextNotContains('Entity test bundle');
    $this->getSession()->getPage()->checkField('Test entity - data table');
    $this->assertSession()->pageTextContains('Entity Test Bundle');

    $this->assertSession()->pageTextNotContains('Name');
    $this->assertSession()->pageTextNotContains('Test translatable text-field');
    $this->getSession()->getPage()->checkField('Entity Test Bundle');
    $this->assertSession()->pageTextContains('Name');
    $this->assertSession()->pageTextContains('Test translatable text-field');

    $this->getSession()->getPage()->checkField('Name');
    $this->getSession()->getPage()->checkField('Test translatable text-field');

    $this->submitForm([], 'Save configuration');

    $this->assertSession()->pageTextContains('After removing / adding new entity types is needed to clear caches.');

    $entity_types = $this->config('entity_translation_sync.settings')->get('entity_types');
    $this->assertIsArray($entity_types);
    $this->assertEquals(
      [
        'entity_test_mul' => [
          'bundles' => [
            'entity_test_mul' => [
              'fields' => [
                'name',
                'field_etsync_text',
              ],
            ],
          ],
        ],
      ], $entity_types, 'Entity synchronizable types are saved correctly');
  }

  /**
   * Test configuration page saves empty data if no data is sent.
   */
  public function testConfigurationWithoutAddingValues() {
    $this->drupalLogin($this->administratorUser);
    $this->drupalGet('/admin/config/regional/entity-translation-sync');
    $this->submitForm([], 'Save configuration');

    $this->assertSession()->pageTextNotContains('After removing / adding new entity types is needed to clear caches.');

    $entity_types = $this->config('entity_translation_sync.settings')->get('entity_types');
    $this->assertIsArray($entity_types);
    $this->assertEmpty($entity_types);
  }

  /**
   * Ensure there is error when a bundle is enabled without enabling fields.
   */
  public function testConfigurationWithoutEnablingFields() {
    $this->drupalLogin($this->administratorUser);
    $this->drupalGet('/admin/config/regional/entity-translation-sync');
    $this->getSession()->getPage()->checkField('Test entity - data table');
    $this->getSession()->getPage()->checkField('Entity Test Bundle');

    $this->submitForm([], 'Save configuration');

    $this->assertSession()->pageTextContains('Bundle "Entity Test Bundle" is enabled for synchronization but its fields are not enabled.');
  }

  /**
   * Test page works well in a site without translatable entities.
   *
   * When there aren't translatable entities a message must be displayed.
   */
  public function testConfigurationWithoutTranslatableEntities() {
    $this->disableTranslation();
    $this->drupalLogin($this->administratorUser);
    $this->drupalGet('/admin/config/regional/entity-translation-sync');
    $this->assertSession()->pageTextContains('Entity Translation Sync settings');
    $this->assertSession()->pageTextContains('There are not translatable entities enabled in the site.');
  }

  /**
   * Disable translation.
   *
   * It is done for tests that needs to check behaviours when there aren't
   * translatable entity types.
   */
  protected function disableTranslation() {
    $this->container->get('content_translation.manager')->setEnabled('entity_test_mul', 'entity_test_mul', FALSE);
  }

}
