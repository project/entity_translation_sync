# Entity Translation Sync

The Entity Translation Sync module provides a page of translatable entities that allow synchronizing values of translatable fields among several translations. This module saves time in changing field values in content with translations.

Use case: there is a node that has a media translatable and 5 translations. It is needed to change the media in the 5 translations to the same value. Instead of going to the translation page of each language, the synchronization process can be done through the page provided by this module.

## How it works

To configure it:

 - Enable which entity fields, bundles, and fields are supported at Configuration >> Regional and Language >> Entity translation sync
 - Clear caches. This will be needed every time an entity type is enabled / disabled.
 - Assign the permissions to the roles that will be able to to the entity synchronization. There exists a permission for each entity type (s.e.: Synchronize node translation), and also a global one (Synchronize any entity translation).

After configuring the module, it will appear a tab named 'Entity translation sync' in the translatable supported entities. On that page, there is a form that allows selecting which fields from the current language will be translated to which languages.
