<?php

namespace Drupal\entity_translation_sync;

use Drupal\Core\Config\ConfigBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * For each specific entity type gets its permission.
 */
final class EntityTranslationSyncPermissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * Used to get the list of enabled entity type IDs.
   *
   * @var \Drupal\Core\Config\ConfigBase
   */
  protected ConfigBase $config;

  /**
   * Used to load the enabled entity types.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs the permissions class.
   *
   * @param \Drupal\Core\Config\ConfigBase $config
   *   Module configuration.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(ConfigBase $config, EntityTypeManagerInterface $entity_type_manager) {
    $this->config = $config;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
    $config_factory = $container->get('config.factory');
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $container->get('entity_type.manager');
    return new static($config_factory->get('entity_translation_sync.settings'), $entity_type_manager);
  }

  /**
   * Gets all permissions that allow sync each entity type.
   *
   * @return array
   *   Key is the permission name and value is array with title and description.
   */
  public function permissions() {
    $permissions = [];
    /** @var string $entity_type_id */
    foreach (array_keys($this->config->get('entity_types') ?? []) as $entity_type_id) {
      $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
      if ($entity_type instanceof EntityTypeInterface) {
        $args = ['@entity_type' => $entity_type->getLabel()];
        $permissions[sprintf('synchronize %s translation', $entity_type_id)] = [
          'title' => $this->t('Synchronize @entity_type translation', $args),
          'description' => $this->t('Users with this permission will be able to synchronize @entity_type attributes among its translations.', $args),
        ];
      }
    }
    return $permissions;
  }

}
