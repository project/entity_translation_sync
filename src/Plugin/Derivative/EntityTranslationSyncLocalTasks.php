<?php

namespace Drupal\entity_translation_sync\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Config\ConfigBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides dynamic local tasks for editorial access management.
 */
final class EntityTranslationSyncLocalTasks extends DeriverBase implements ContainerDeriverInterface {
  use StringTranslationTrait;

  /**
   * The base plugin ID.
   *
   * @var string
   */
  protected $basePluginId;

  /**
   * Editorial access manager.
   */
  protected ConfigBase $config;

  /**
   * Constructs a new EditorialManagerLocalTasks.
   *
   * @param string $base_plugin_id
   *   The base plugin ID.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The translation manager.
   */
  public function __construct($base_plugin_id, ConfigFactoryInterface $config_factory, TranslationInterface $string_translation) {
    $this->basePluginId = $base_plugin_id;
    $this->config = $config_factory->get('entity_translation_sync.settings');
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $base_plugin_id,
      $container->get('config.factory'),
      $container->get('string_translation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    // Create tabs for all possible entity types.
    $entity_types = array_keys($this->config->get('entity_types') ?? []);
    foreach ($entity_types as $entity_type_id) {
      // Find the route name for the translation overview.
      $translation_route_name = "entity.$entity_type_id.entity_translation_sync";

      $base_route_name = "entity.$entity_type_id.canonical";
      $this->derivatives[$translation_route_name] = [
        'entity_type' => $entity_type_id,
        'title' => $this->t('Entity translation sync'),
        'route_name' => $translation_route_name,
        'base_route' => $base_route_name,
      ] + $base_plugin_definition;
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
