<?php

namespace Drupal\entity_translation_sync\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ConfigBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Checks access to assigned content pages.
 */
class EntityTranslationSyncAccessChecker implements AccessInterface {

  /**
   * Used to check the current entity type and bundle are enabled.
   *
   * @var \Drupal\Core\Config\ConfigBase
   */
  protected ConfigBase $config;

  /**
   * Constructs the access checker.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Configuration factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('entity_translation_sync.settings');
  }

  /**
   * Check access to editorial manager assignment page.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   Route match service.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(RouteMatchInterface $route_match, AccountInterface $account) {
    $entity_types_config = $this->config->get('entity_types');
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $route_match->getParameter($route_match->getParameter('entity_type_id'));
    if ($entity instanceof ContentEntityInterface && !empty($entity_types_config[$entity->getEntityTypeId()]['bundles'][$entity->bundle()])) {

      $permissions = [
        'synchronize any entity translation',
        sprintf('synchronize %s translation', $entity->getEntityTypeId()),
      ];

      return AccessResult::allowedIfHasPermissions($account, $permissions, 'OR');
    }

    return AccessResult::forbidden();
  }

}
