<?php

namespace Drupal\entity_translation_sync\EventSubscriber;

use Drupal\Core\Config\ConfigBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Route subscriber.
 */
class EntityTranslationSyncRouteSubscriber extends RouteSubscriberBase {

  /**
   * Gets the enabled entities for doing translation sync.
   *
   * @var \Drupal\Core\Config\ConfigBase|\Drupal\Core\Config\ImmutableConfig
   */
  protected ConfigBase $config;

  /**
   * Loads the entity definition to generate routes based on link templates.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs the subscriber.
   */
  public function __construct(ConfigFactoryInterface $configFactory, EntityTypeManagerInterface $entityTypeManager) {
    $this->config = $configFactory->get('entity_translation_sync.settings');
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $entity_types = array_keys($this->config->get('entity_types') ?? []);
    /** @var string $entity_type_id */
    foreach ($entity_types as $entity_type_id) {
      $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);

      if ($entity_type instanceof EntityTypeInterface && $entity_type->hasLinkTemplate('drupal:entity-translation-sync')) {
        // Inherit admin route status from edit route, if exists.
        $is_admin = FALSE;
        $route_name = "entity.$entity_type_id.edit_form";
        if ($edit_route = $collection->get($route_name)) {
          $is_admin = (bool) $edit_route->getOption('_admin_route');
        }
        /** @var string $link_template */
        $link_template = $entity_type->getLinkTemplate('drupal:entity-translation-sync');
        $route = new Route(
          $link_template,
          [
            '_form' => '\Drupal\entity_translation_sync\Form\EntityTranslationSyncForm',
            'entity_type_id' => $entity_type_id,
          ],
          [
            '_entity_access' => $entity_type_id . '.view',
            '_entity_translation_sync_access' => 'TRUE',
          ],
          [
            'parameters' => [
              $entity_type_id => [
                'type' => sprintf('entity:%s', $entity_type_id),
              ],
            ],
            '_admin_route' => $is_admin,
          ]
        );
        $route_name = "entity.$entity_type_id.entity_translation_sync";
        $collection->add($route_name, $route);
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    $events = parent::getSubscribedEvents();

    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -219];

    return $events;
  }

}
