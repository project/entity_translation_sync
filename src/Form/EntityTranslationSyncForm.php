<?php

namespace Drupal\entity_translation_sync\Form;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to synchronize entities attributes.
 */
class EntityTranslationSyncForm extends FormBase {

  /**
   * Used to get the entity in the current language.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected EntityRepositoryInterface $entityRepository;

  /**
   * Used to display language labels in messages.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    /** @var \Drupal\Core\Entity\EntityRepositoryInterface entityRepository */
    $entity_repository = $container->get('entity.repository');
    $instance->entityRepository = $entity_repository;
    /** @var \Drupal\Core\Language\LanguageManagerInterface $language_manager */
    $language_manager = $container->get('language_manager');
    $instance->languageManager = $language_manager;
    return $instance;
  }

  /**
   * Get the entity that will be synchronized.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The entity in the current language.
   */
  protected function getEntity() : ContentEntityInterface {
    $route_match = $this->getRouteMatch();
    $entity_type_id = $route_match->getParameter('entity_type_id');
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $route_match->getParameter($entity_type_id);
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $this->entityRepository->getTranslationFromContext($entity);
    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_translation_sync_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $entity_types_config = $this->config('entity_translation_sync.settings')->get('entity_types');
    $fields = $entity_types_config[$entity->getEntityTypeId()]['bundles'][$entity->bundle()]['fields'];
    $languages_to_sync = $this->getLanguagesToSync($entity);
    if (empty($languages_to_sync)) {
      return [
        '#markup' => $this->t("This entity do not have translation to sync its fields with."),
      ];
    }
    $form = [];
    $form['entity_attributes_sync'] = [
      '#type' => 'table',
      '#tree' => FALSE,
      '#header' => [
        $this->t('Attributes'),
        sprintf('%s (%s)', $entity->language()->getName(), $this->t('Selected language')),
      ],
      '#rows' => [],
    ];
    foreach ($languages_to_sync as $language) {
      $form['entity_attributes_sync']['#header'][] = $language->getName();
    }

    foreach ($fields as $field_name) {
      try {
        $field = $entity->get($field_name);
        if (($field->access('view') || $field->access('edit')) && !$field->isEmpty()) {
          $field_definition = $field->getFieldDefinition();
          $row = [
            'field_name' => [
              '#markup' => sprintf('%s (%s)', $field_definition->getLabel(), $field_definition->getName()),
            ],
            'original_value' => $entity->get($field_name)->view(),
          ];
          foreach ($languages_to_sync as $language) {
            if ($field->access('edit')) {
              $row[$language->getId()] = [
                '#type' => 'checkbox',
                '#name' => sprintf('entity_attributes_sync[%s][%s]', $language->getId(), $field_name),
              ];
            }
          }
          $form['entity_attributes_sync'][$field_name] = $row;
        }
      }
      catch (\InvalidArgumentException $e) {
        $this->logger('entity_translation_sync')->error(
          sprintf(
            'Error getting field %s for "%s" entity type: %s',
            $field_name,
            $entity->getEntityType()->getLabel(),
            $e->getMessage(),
          )
        );
      }
    }

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Sync'),
      ],
    ];
    return $form;
  }

  /**
   * Get the list of languages to sync the entity to.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity to get its languages.
   *
   * @return array
   *   All languages except the current entity language.
   */
  protected function getLanguagesToSync(ContentEntityInterface $entity) {
    return array_filter($entity->getTranslationLanguages(), function (LanguageInterface $language) use ($entity) {
      return $language->getId() != $entity->language()->getId();
    });
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $entity_attributes_sync = $form_state->getValue('entity_attributes_sync');
    if (empty($entity_attributes_sync)) {
      $form_state->setErrorByName('entity_attributes_sync', 'No attributes has been selected to sync.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity_attributes_sync = $form_state->getValue('entity_attributes_sync');
    $this->createBatch($entity_attributes_sync);
  }

  /**
   * Generates the batch.
   *
   * @param array $entity_attributes_sync
   *   Entity attrbutes to sync grouped by language.
   */
  protected function createBatch(array $entity_attributes_sync) {
    $entity = $this->getEntity();
    $batch = [
      'title' => $this->t('Synchronizing entity translation attributes'),
      'finished' => [$this, 'batchFinishedMessage'],
    ];
    $batch['operations'][] = [
      [$this, 'batchSetup'],
      [
        $entity,
      ],
    ];
    foreach ($entity_attributes_sync as $language => $fields) {
      if ($entity->hasTranslation($language) && !empty($fields)) {
        $batch['operations'][] = [
          [$this, 'syncEntityTranslationBatchCallback'],
          [
            $language,
            array_keys(array_filter($fields)),
          ],
        ];
      }
    }
    $batch['operations'][] = [
      [$this, 'saveSynchronizedEntity'],
      [],
    ];
    batch_set($batch);
  }

  /**
   * Ensures the entity is available at every batch step.
   */
  public function batchSetup(ContentEntityInterface $entity, &$context) {
    $context['results']['entity'] = $entity;
  }

  /**
   * Batch callback to do the entity synchronization.
   */
  public function syncEntityTranslationBatchCallback(string $language, array $fields, &$context) {
    try {
      $entity = $context['results']['entity'];
      if ($entity instanceof ContentEntityInterface) {
        $this->syncEntityTranslation($entity, $language, $fields);
        $context['results']['success'][] = $language;
      }
    }
    catch (\Exception $exception) {
      $context['results']['failed'][] = $language;
      $this->logger('entity_translation_sync')->error(
        sprintf(
          'Error synchronizing entity of "%s" entity type with id "%s": %s',
          $entity->getEntityType()->getLabel(),
          $entity->id(),
          $exception->getMessage(),
        )
      );
    }
  }

  /**
   * Syncrhonize entity translation.
   *
   * Translation is not saved because
   * it will be saved at ::saveSynchronizedEntity.
   */
  public function syncEntityTranslation(ContentEntityInterface $entity, string $language, array $fields) {
    $translation = $entity->getTranslation($language);
    if (!empty($fields)) {
      foreach ($fields as $field) {
        $translation->set($field, $entity->get($field)->getValue());
      }
    }
  }

  /**
   * Saves the entity after all the synchronizations have been done.
   */
  public function saveSynchronizedEntity(&$context) {
    $context['results']['entity']->save();
  }

  /**
   * Shows errors and successes when the batch finishes.
   */
  public function batchFinishedMessage($success, $results, $operations) {
    if (!empty($results['success'])) {
      $this->messenger()->addMessage($this->t('Entity has been synchronized successfully to: :languages', [
        ':languages' => implode(', ', array_map(function (string $langcode) {
          /** @var \Drupal\Core\Language\LanguageInterface $language */
          $language = $this->languageManager->getLanguage($langcode);
          return $language->getName();
        }, $results['success'])),
      ]));
    }

    if (!empty($results['failed'])) {
      $this->messenger()->addError($this->t('Entity has failed synchronization to: :languages. Please review the log messages or contact with an administrator.', [
        ':languages' => implode(', ', array_map(function (string $langcode) {
          /** @var \Drupal\Core\Language\LanguageInterface $language */
          $language = $this->languageManager->getLanguage($langcode);
          return $language->getName();
        }, $results['failed'])),
      ]));
    }
  }

}
