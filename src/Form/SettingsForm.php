<?php

namespace Drupal\entity_translation_sync\Form;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Entity Translation Sync settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Used to get all translatable entity types.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Used to get entity type existing bundles.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected EntityTypeBundleInfoInterface $entityTypeBundleInfo;

  /**
   * Used to retrieve all the fields of each bundle.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $container->get('entity_type.manager');
    $instance->entityTypeManager = $entity_type_manager;
    /** @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info */
    $entity_type_bundle_info = $container->get('entity_type.bundle.info');
    $instance->entityTypeBundleInfo = $entity_type_bundle_info;
    /** @var \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager */
    $entity_field_manager = \Drupal::service('entity_field.manager');
    $instance->entityFieldManager = $entity_field_manager;
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_translation_sync_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['entity_translation_sync.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $entity_types = $this->config('entity_translation_sync.settings')->get('entity_types') ?? [];

    $form['entity_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled entity types'),
      '#default_value' => array_keys($entity_types),
    ];

    $form['fields'] = [
      '#tree' => TRUE,
    ];

    foreach ($this->entityTypeManager->getDefinitions() as $entity_type) {
      if ($entity_type->isTranslatable()) {
        $bundles = $this->entityTypeBundleInfo->getBundleInfo($entity_type->id());
        foreach ($bundles as $bundle_id => $bundle_info) {
          if ($bundle_info['translatable']) {
            if (!isset($form['entity_types']['#options'][$entity_type->id()])) {
              $form['entity_types']['#options'][$entity_type->id()] = $entity_type->getLabel();
            }

            if (!isset($form['fields']['entity_types'][$entity_type->id()]['label'])) {
              $form['fields']['entity_types'][$entity_type->id()]['label'] = [
                '#type' => 'item',
                '#markup' => '<h4>' . $entity_type->getLabel() . '</h4>',
                '#states' => [
                  'visible' => [
                    ':input[name="entity_types[' . $entity_type->id() . ']"]' => ['checked' => TRUE],
                  ],
                ],
              ];
            }

            $form['fields']['entity_types'][$entity_type->id()][$bundle_id]['enabled'] = [
              '#type' => 'checkbox',
              '#title' => $bundle_info['label'],
              '#attributes' => [
                'class' => ['entity-translation-sync-settings-bundle-enabled'],
              ],
              '#states' => [
                'visible' => [
                  ':input[name="entity_types[' . $entity_type->id() . ']"]' => ['checked' => TRUE],
                ],
              ],
              '#default_value' => isset($entity_types[$entity_type->id()]['bundles'][$bundle_id]),
            ];

            $fields = $this->entityFieldManager->getFieldDefinitions($entity_type->id(), $bundle_id);
            $bundle_fields = $entity_types[$entity_type->id()]['bundles'][$bundle_id]['fields'] ?? [];
            foreach ($fields as $field) {
              $field_config = $field->getConfig($bundle_id);
              // Do not support entity reference revisions yet.
              if ($field_config->isTranslatable() && $field_config->getType() != 'entity_reference_revisions') {
                $form['fields']['entity_types'][$entity_type->id()][$bundle_id]['fields'][$field->getName()] = [
                  '#type' => 'checkbox',
                  '#title' => $field->getLabel(),
                  '#attributes' => [
                    'class' => ['entity-translation-sync-settings-bundle-field'],
                  ],
                  '#states' => [
                    'visible' => [
                      ':input[name="fields[entity_types][' . $entity_type->id() . '][' . $bundle_id . '][enabled]"]' => ['checked' => TRUE],
                    ],
                  ],
                  '#default_value' => in_array($field->getName(), $bundle_fields),
                ];
              }
            }
          }

        }
      }

      $form['#attached']['library'] = 'entity_translation_sync/settings_form';
    }

    if (empty($form['entity_types']['#options'])) {
      return [
        '#type' => 'container',
        'message' => [
          '#markup' => $this->t('There are not translatable entities enabled in the site. Set it up here:@separator', ['@separator' => ' ']),
        ],
        'url' => [
          '#type' => 'link',
          '#title' => $this->t('Content language and translation'),
          '#url' => Url::fromRoute('language.content_settings_page'),
        ],
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $enabled_entity_types = $form_state->getValue('entity_types');
    foreach ($form_state->getValue(['fields', 'entity_types']) as $entity_type => $bundles) {
      $bundles_info = $this->entityTypeBundleInfo->getBundleInfo($entity_type);
      if (!empty($enabled_entity_types[$entity_type])) {
        foreach ($bundles as $bundle => $bundle_info) {
          if (is_array($bundle_info) and !empty($bundle_info['enabled'])) {
            $enabled_fields = array_keys(array_filter($bundle_info['fields']));
            if (empty($enabled_fields)) {
              $form_state->setError($form['fields']['entity_types'][$entity_type][$bundle], $this->t(
                'Bundle ":bundle_label" is enabled for synchronization but its fields are not enabled.', [
                  ':bundle_label' => $bundles_info[$bundle]['label'],
                ]
              ));
            }
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('entity_translation_sync.settings');
    $entity_types = [];

    $old_entity_types = array_keys($config->get('entity_types') ?? []);
    $enabled_entity_types = $form_state->getValue('entity_types');
    foreach ($form_state->getValue(['fields', 'entity_types']) as $entity_type => $bundles) {
      if (!empty($enabled_entity_types[$entity_type])) {
        foreach ($bundles as $bundle => $bundle_info) {
          if (is_array($bundle_info) and !empty($bundle_info['enabled'])) {
            $entity_types[$entity_type]['bundles'][$bundle] = [
              'fields' => array_keys(array_filter($bundle_info['fields'])),
            ];
          }
        }
      }
    }
    $config->set('entity_types', $entity_types)->save();

    $new_entity_types = array_keys($entity_types);
    if (count(array_intersect($old_entity_types, $new_entity_types)) != count($new_entity_types)
      || count($new_entity_types) != count($old_entity_types)) {
      $this->messenger()->addWarning('After removing / adding new entity types is needed to clear caches.');
    }
  }

}
